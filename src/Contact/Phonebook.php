<?php

namespace App\Contact;

use App\Model\Database as DB;
use App\Utility\Direction;
use App\Utility\Message;

class Phonebook extends DB{
    public $id;
    public $name;
    public $mobile;
    public $group;
    public $img_path;
    
    public $table;

    public function __construct() {
        parent::__construct();
        $this->table = "phonebook";
    }

    public function index(){
        
        $records = $this->findAll();
        return $records;
    }
    
    public function store($data = FALSE){
        if(!empty($data)){
            $data['name'] = implode(" ", $data['name']);
        
            $has_img = !(empty($data['profile_picture']['name']));
            if($has_img){
                $img_name = $data['profile_picture']['name'];
                $tmp_name = $data['profile_picture']['tmp_name'];

                $this->img_path = '/SecurePhonebook/resource/uploads/'.$img_name;
                $data['profile_picture'] = $this->img_path;
            } else{
                $data['profile_picture'] = "/SecurePhonebook/resource/uploads/default.jpg";
            }

            $result = $this->insertInto($data);

            if($result){

                if($has_img){
                    $move_to = $_SERVER['DOCUMENT_ROOT'].'/SecurePhonebook/resource/uploads/'.$img_name;
                    move_uploaded_file($tmp_name, $move_to);
                }
                Message::message("<div class='alert alert-success'>New contact added successfully!</div>");
            } else{
                Message::message("<div class='alert alert-danger'>Unable to add new contact!! Try again later :(</div>");
            }

            Direction::redirect();   
        } else{
            Message::message("<div class='alert alert-danger'>Stop messing with insertion! You can't store via URL!</div>");
            Direction::redirect();
        }
    }
    
    public function show($id = FALSE){
        
        $records = $this->find($id);
        return $records;
    }
    
    public function update($data = FALSE){
        
        $old_pic = $this->show($data['id']);
        
        $this->id = $data['id'];
        $this->name = implode(" ", $data['name']);
        $this->mobile = $data['mobile'];
        $this->group = $data['group'];
        
        $has_img = !(empty($data['profile_picture']['name']));
        if($has_img){
            $img_name = $data['profile_picture']['name'];
            $tmp_name = $data['profile_picture']['tmp_name'];
            
            $this->img_path = '/SecurePhonebook/resource/uploads/'.$img_name;
        } else{
            $this->img_path = "/SecurePhonebook/resource/uploads/default.jpg";
        }
        
        $query = "";
        
        if($has_img){
            $query = "UPDATE `miniphonebook`.`phonebook` SET `name` = '".$this->name."', `mobile` = '".$this->mobile."', `group` = '".$this->group."', `profile_picture` = '".$this->img_path."' WHERE `phonebook`.`id` = ".$this->id;
            $delete_this_file = $_SERVER['DOCUMENT_ROOT'].$old_pic->profile_picture;
            
            if(!strstr($old_pic->profile_picture, 'default.jpg')){
                unlink($delete_this_file);
            }
            
        } else{
            $query = "UPDATE `miniphonebook`.`phonebook` SET `name` = '".$this->name."', `mobile` = '".$this->mobile."', `group` = '".$this->group."' WHERE `phonebook`.`id` = ".$this->id;
        }
        
        $result = mysql_query($query);
        
        if($result){
            $move_to = $_SERVER['DOCUMENT_ROOT'].'/SecurePhonebook/resource/uploads/'.$img_name;
            move_uploaded_file($tmp_name, $move_to);
            
            Message::message("<div class='alert alert-success'>Profile picture updated successfully!</div>");
        } else{
            Message::message("<div class='alert alert-danger'>Can not update profile picture!! Try again later :(</div>");
        }
        
        Direction::redirect();      
    }
    
    public function delete($id = NULL){
        
        if(is_null($id)){
            Message::message("<div class='alert alert-danger'>Sorry, no id available!! Try again later :(</div>");
            return Direction::redirect();
        } else{
            
            $contact = $this->show($id);
            $delete_this_file = $_SERVER['DOCUMENT_ROOT'].$contact->profile_picture;
            
            if(!strstr($delete_this_file, 'default.jpg')){
                unlink($delete_this_file);
            }
            
            $query = "DELETE FROM `miniphonebook`.`phonebook` WHERE `phonebook`.`id` =".$id;
            $result = mysql_query($query);
            
            if($result){
                Message::message("<div class='alert alert-success'>Contact deleted successfully</div>");
            } else{
                Message::message("<div class='alert alert-danger'>Error deleting contact! Try again later(</div>");
            }
            Direction::redirect();
        }
    }
}