<?php

namespace App\Utility;

class Direction {
    
    static public function redirect($url = "index.php"){
        header("Location: ".$url);
    }
}
