<?php

namespace App\User;
use App\Model\Database as DB;
use App\Utility\Direction;
use App\Utility\Message;

class User extends DB{
    
    public $id;
    public $email;
    public $password;


    public function __construct() {
        parent::__construct();
        $this->table = "user";
    }
    
    public function store($data = FALSE){
        $userdata = array();
        if($data['password'] == $data['confirm_password']){
            $userdata['email'] = $data['email'];
            $userdata['password'] = md5($data['password']);
            
            $result = $this->insertInto($userdata);
            if($result){
                Message::message("<div class='alert alert-success'>You've successfully registered! You can sign in using Email: <b>{$data['email']}</b> and Password: <b>{$data['password']}</b></div>");
            }
        } else{
            Message::message("<div class='alert alert-danger'>Password and confirmation password missmatched!</div>");
            return Direction::redirect("../../index.php");
        }
        
        return Direction::redirect("../../index.php");
    }
}
